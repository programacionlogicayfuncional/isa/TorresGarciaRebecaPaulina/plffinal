(ns plffinal.core)
;Recibí consejos de algunos compañeros es por eso que no subí un video
;ya que no es por completo de mi autoría 
;Problema 1
(defn norte
  [x]
  (count (apply vector (filter #(= % \^) (apply vector x)))))

(defn sur
  [x]
  (count (apply vector (filter #(= % \>) (apply vector x)))))

(defn este
  [x]
  (count (apply vector (filter #(= % \<) (apply vector x)))))

(defn oeste
  [x]
  (count (apply vector (filter #(= % \v) (apply vector x)))))


(defn pasos [x]
  (- (+ (* 2 (norte x)) (oeste x)) (+ (* 2 (sur x)) (este x))))


(defn regresa-al-punto-de-origen?
  [x]
  (zero? (pasos x)))



(regresa-al-punto-de-origen? "")
(regresa-al-punto-de-origen? [])
(regresa-al-punto-de-origen? (list))
(regresa-al-punto-de-origen? "><")
(regresa-al-punto-de-origen? (list \> \<))
(regresa-al-punto-de-origen? "v^")
(regresa-al-punto-de-origen? [\v \^])
(regresa-al-punto-de-origen? "^>v<")
(regresa-al-punto-de-origen? (list \^ \> \v \<))
(regresa-al-punto-de-origen? "<<vv>>^^")
(regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])

(regresa-al-punto-de-origen? ">")
(regresa-al-punto-de-origen? (list \>))
(regresa-al-punto-de-origen? "<^")
(regresa-al-punto-de-origen? [\< \^])
(regresa-al-punto-de-origen? ">>><<")
(regresa-al-punto-de-origen? (list \> \> \> \< \<))
(regresa-al-punto-de-origen? [\v \v \^ \^ \^])